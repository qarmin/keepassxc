### Compilation in Docker

```
docker run -it ubuntu:rolling /bin/bash

sed -Ei 's/^# deb-src /deb-src /' /etc/apt/sources.list

export DEBIAN_FRONTEND=noninteractive DEBCONF_NONINTERACTIVE_SEEN=true

apt update
apt install -y git
git clone https://github.com/keepassxreboot/keepassxc.git
cd keepassxc

#Z#
apt build-dep -y keepassxc
apt install -y qtbase5-dev qtbase5-private-dev qttools5-dev qttools5-dev-tools libqt5svg5-dev libgcrypt20-dev libargon2-0-dev libqrencode-dev libsodium-dev zlib1g-dev
apt install -y libxi-dev libxtst-dev libqt5x11extras5-dev libyubikey-dev libykpers-1-dev libcurl4-openssl-dev libquazip5-dev libreadline-dev
mkdir build
cd build
cmake -DWITH_XC_ALL=ON ..

make -j8

```

### QTCreator Includes
```

```
